# Doxygen filter for ASN.1 syntax

The ASN.1 Doxygen filter is a python3 based parser for the ASN.1 syntax. It is
intended to be used with the documentation generation utility, [Doxygen][1].
This filter is distributed under the MIT license.

## Installation:
Add this repo as a submodule in the root folder of your ASN.1 source files
using:
```sh
git submodule add https://bitbucket.org/raashid_ansari/asn1-doxygen-filter
```
or clone this repo to the folder where you have the source files using:
```sh
git clone https://bitbucket.org/raashid_ansari/asn1-doxygen-filter.git
```
For installation instructions of doxygen, please refer Doxygen's [download][5]
page.

## Usage:
**NOTE:** This repo **MUST** be in your ASN.1 source root folder. This will make
sure that your doxygen configuration file is at the correct path for doxygen to
create documentation properly.

#### Linux:
Use the following commands (in your terminal) to run Doxygen on your ASN.1 files
commented using the [Commenting Style Guide][2]:
```sh
cd <your_src_files_folder>/asn1-doxygen-filter/
doxygen <doxygen_config_file_name>
```
You may also use the GUI frontend tool for Doxygen called [doxywizard][3] to
run this filter using the following command in your terminal:
```sh
cd <your_src_files_folder>/asn1-doxygen-filter/
doxywizard <doxygen_config_file_name>
```
Go to the "Run" tab and hit "Run Doxygen". Once its operation is completed,
you may see the output by hitting the "Show HTML Output" button on the same tab.

NOTE: You may have to install `doxywizard` utility separately on your Linux
distribution.

#### Mac:
This tool is named "Doxygen" on Mac and the binary available for download
contains the GUI [doxywizard][3] utility.

1. Open Doxygen GUI.
2. From the system's menu, go to "File>Open", select the doxy.cfg file from this
repo's folder.
3. Go to the "Run" tab and hit "Run Doxygen".
4. Once its operation is completed, you may see the output by hitting the "Show
HTML Output" button on the same tab.

#### Windows:
Usage on Windows has not been tested but the instructions for Mac should work.

## Debugging:
For debugging the filter itself, Use the following command to run the filter
script without Doxygen:
```sh
./asn1-doxygen-filter.py <filename>
```
NOTE: This will not generate the HTML documentation for you but will give you
an idea of how your file is parsed and fed to Doxygen.

## Dependencies
*   python3 (3.5.x)
*   doxygen (1.8.1x)

## References
Please refer the [Commenting Style][2] file to see how
to comment your ASN.1 files so that this filter works properly.

Also, refer the [sample doxygen configuration][4] file for a typical
Doxygen configuration to be used with this filter.

[1]: http://www.stack.nl/~dimitri/doxygen/index.html
[2]: https://bitbucket.org/raashid_ansari/asn1-doxygen-filter/src/master/commenting-style.md
[3]: https://www.stack.nl/~dimitri/doxygen/manual/doxywizard_usage.html
[4]: https://bitbucket.org/raashid_ansari/asn1-doxygen-filter/src/master/doxy.cfg
[5]: http://www.stack.nl/~dimitri/doxygen/download.html
